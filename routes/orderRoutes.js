const express = require('express');
const auth = require('../auth');
const orderController = require('../controllers/orderControllers');
const router = express.Router();

router.post('/checkout', auth.verify, (req, res) => {
  let userId = auth.decode(req.headers.authorization).id;
  orderController.checkout(userId).then((result) => res.send(result));
});

router.get('/user', auth.verify, (req, res) => {
  let userId = auth.decode(req.headers.authorization).id;
  orderController.getOrder(userId).then((result) => res.send(result));
});

router.get('/get-all', auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  orderController.getAllOrders(isAdmin).then((result) => res.send(result));
});

module.exports = router;
