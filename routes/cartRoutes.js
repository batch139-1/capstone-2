const express = require('express');
const auth = require('../auth');
const cartController = require('../controllers/cartControllers');
const router = express.Router();

router.get('/', auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  cartController.getAllCarts(isAdmin).then((result) => res.send(result));
});

router.post('/add', auth.verify, (req, res) => {
  let userId = auth.decode(req.headers.authorization).id;
  cartController.addToCart(userId, req.body).then((result) => res.send(result));
});

module.exports = router;
