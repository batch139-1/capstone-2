const express = require('express');
const auth = require('../auth');
const userController = require('../controllers/userControllers');
const router = express.Router();

router.get('/', (req, res) => {
  userController.getAllUsers().then((result) => res.send(result));
});

router.post('/register', (req, res) => {
  userController.registerUser(req.body).then((result) => res.send(result));
});

router.post('/login', (req, res) => {
  userController.userLogin(req.body).then((result) => res.send(result));
});

router.put('/:id/setAsAdmin', auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  userController
    .setAsAdmin(req.params.id, isAdmin)
    .then((result) => res.send(result));
});

module.exports = router;
