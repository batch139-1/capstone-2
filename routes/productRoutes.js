const express = require('express');
const auth = require('../auth');
const productController = require('../controllers/productControllers');
const router = express.Router();

router.post('/add-product', auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  productController
    .addProduct(req.body, isAdmin)
    .then((result) => res.send(result));
});

router.get('/', (req, res) => {
  productController.getActiveProducts().then((result) => res.send(result));
});

router.get('/:id', (req, res) => {
  productController
    .getProductById(req.params.id)
    .then((result) => res.send(result));
});

router.put('/:id/update', auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  productController
    .updateProduct(req.body, req.params.id, isAdmin)
    .then((result) => res.send(result));
});

router.put('/:id/archive', auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  productController
    .archiveProduct(req.params.id, isAdmin)
    .then((result) => res.send(result));
});

module.exports = router;
