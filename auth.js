const jwt = require('jsonwebtoken');
const secret = 'eCommerceApp';

module.exports.createAccessToken = (user) => {
  const { _id, email, isAdmin } = user;
  const data = {
    id: _id,
    email: email,
    isAdmin: isAdmin,
  };
  return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (token) {
    return jwt.verify(token.slice(7, token.length), secret, (err) => {
      if (err) {
        return res.send({ auth: 'failed' });
      } else {
        next();
      }
    });
  }
};

module.exports.decode = (token) => {
  if (token) {
    let decoded = jwt.decode(token.slice(7, token.length), { complete: true });
    return decoded.payload;
  }
};
