const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cartSchema = new Schema({
  userId: {
    type: String,
  },
  products: [
    {
      productId: {
        type: String,
      },
      name: String,
      quantity: {
        type: Number,
        required: true,
        min: [1, 'Quantity can not be less then 1.'],
        default: 1,
      },
      price: Number,
    },
  ],
  totalAmount: {
    type: Number,
    required: true,
    default: 0,
  },
});

module.exports = Cart = mongoose.model('Cart', cartSchema);
