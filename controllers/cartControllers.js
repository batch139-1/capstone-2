const Cart = require('../models/Cart');
const auth = require('../auth');
const Product = require('../models/Product');

module.exports.getAllCarts = (admin) => {
  if (admin == false) {
    return false;
  } else {
    return Cart.find().then((result) => result);
  }
};

module.exports.addToCart = async (userId, reqBody) => {
  const { productId, quantity } = reqBody;
  try {
    let cart = await Cart.findOne({ userId: userId });
    let product = await Product.findById({ _id: productId });
    if (!product) {
      return `no product`;
    }
    const price = product.price;
    const name = product.title;
    if (cart) {
      let productIndex = cart.products.findIndex(
        (p) => p.productId == productId
      );

      if (productIndex > -1) {
        //product exists in the cart, update the quantity
        let productItem = cart.products[productIndex];
        productItem.quantity += quantity;
        cart.totalAmount = productItem.quantity * productItem.price;
      } else {
        //product does not exists in cart, add new item
        cart.products.push({ productId, quantity, name, price });
      }
      cart = await cart.save();
      return `updated cart`;
    } else {
      //no cart for user, create new cart
      await Cart.create({
        userId: userId,
        products: [{ productId, quantity, name, price }],
        totalAmount: quantity * price,
      });
      return `gumawa ng bagong cart`;
    }
  } catch (err) {
    console.log(err);
    return `error`;
  }
};
