const bcrypt = require('bcrypt');
const User = require('../models/User');
const auth = require('../auth');

module.exports.getAllUsers = (admin) => {
  if (admin == false) {
    return false;
  } else {
    return User.find().then((result) => result);
  }
};

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    mobileNo: reqBody.mobileNo,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
  });
  return newUser.save().then((result) => {
    if (!result) {
      return false;
    } else {
      return true;
    }
  });
};

module.exports.userLogin = (reqBody) => {
  const { email, password } = reqBody;
  return User.findOne({ email: email }).then((result) => {
    if (!result) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(password, result.password);
      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};

module.exports.setAsAdmin = (id, admin) => {
  let makeAdmin = {
    isAdmin: true,
  };
  if (admin == false) {
    return false;
  } else {
    return User.findByIdAndUpdate({ _id: id }, makeAdmin).then((result) => {
      if (!result) {
        return `That email does not exist in the database`;
      } else {
        if (!result) {
          return false;
        } else {
          return true;
        }
      }
    });
  }
};
