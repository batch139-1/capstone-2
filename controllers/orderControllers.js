const Order = require('../models/Order');
const Cart = require('../models/Cart');

module.exports.checkout = async (userId) => {
  try {
    let cart = await Cart.findOne({ userId: userId });
    if (cart) {
      const { userId, products, totalAmount } = cart;
      await Order.create({
        userId: userId,
        products: products,
        totalAmount: totalAmount,
      });
      await Cart.findByIdAndDelete({ _id: cart.id });
      return true;
    } else {
      return `no products in cart`;
    }
  } catch (err) {
    console.log(err);
    return `error`;
  }
};

module.exports.getOrder = (userId) => {
  return Order.findOne({ userId: userId }).then((result) => result);
};

module.exports.getAllOrders = (admin) => {
  if (!admin) {
    return false;
  } else {
    return Order.find().then((result) => result);
  }
};
