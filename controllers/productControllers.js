const Product = require('../models/Product');

module.exports.addProduct = (reqBody, admin) => {
  if (admin == false) {
    return false;
  } else {
    const { name, description, price } = reqBody;
    let newProduct = new Product({
      name: name,
      description: description,
      price: price,
    });
    return newProduct.save().then((result) => {
      if (!result) {
        return false;
      } else {
        return true;
      }
    });
  }
};

module.exports.getActiveProducts = () => {
  return Product.find({ isActive: true }).then((result) => {
    if (!result) {
      return false;
    } else {
      return result;
    }
  });
};

module.exports.getProductById = (id) => {
  return Product.findById({ _id: id }).then((result) => {
    if (!result) {
      return false;
    } else {
      return result;
    }
  });
};

module.exports.updateProduct = (reqBody, id, admin) => {
  if (admin == false) {
    return false;
  } else {
    const { name, description, price } = reqBody;
    let updateProduct = {
      name: name,
      description: description,
      price: price,
    };
    return Product.findByIdAndUpdate({ _id: id }, updateProduct).then(
      (result) => {
        if (!result) {
          return false;
        } else {
          return true;
        }
      }
    );
  }
};

module.exports.archiveProduct = (id, admin) => {
  if (admin == false) {
    return false;
  } else {
    let archiveProduct = {
      isActive: false,
    };
    return Product.findByIdAndUpdate({ _id: id }, archiveProduct).then(
      (result) => {
        if (!result) {
          return false;
        } else {
          return true;
        }
      }
    );
  }
};
